from sqlalchemy import (insert, create_engine, Table, MetaData)

class Paciente:
    '''
    @TODO
    '''
    def __init__(self, id, nombre, apellido_paterno, apellido_materno, region, edad):
        '''
        @TODO
        '''
        self.id = id
        self.nombre = nombre
        self.apellido_paterno = apellido_paterno
        self.apellido_materno = apellido_materno
        self.region = region
        self.edad = edad
    
    @staticmethod
    def connect_db(db='app_db'):
        '''
        @TODO
        '''
        # Definimos las variables de conexión al Cloud SQL
        usuario = "application"
        psw = 'application2021'
        local_ip = '172.26.160.3'
        db = 'app_db'
        cloud_sql_instance_name = 'de-program-team-d-310821:us-central1:application'

        # URL conexión
        #url = 'mysql+pymysql://'+usuario+':'+psw+'@'+local_ip+'/'+db
        url = 'mysql+pymysql://'+usuario+':'+psw+'@/'+db+'?unix_socket=/cloudsql/'+cloud_sql_instance_name
        engine = create_engine(url)
        connection = engine.connect()
        metadata = MetaData()
        return metadata, connection, engine

    def insert(self):
        '''
        @TODO
        '''
        # Llamamos a la función para conectarnos a la DB
        metadata, connection, engine = self.connect_db(self)

        # Insertamos la data
        pacientes = Table('pacientes', metadata, autoload=True, autoload_with=engine)
        stmt = insert(pacientes).values(id=self.id, nombre=self.nombre,
                                    apellido_paterno = self.apellido_paterno, apellido_materno = self.apellido_materno,
                                    edad = self.edad)
        result_proxy = connection.execute(stmt)

        # Retornamos el número de filas insertadas
        return result_proxy.rowcount
